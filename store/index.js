import gql from 'graphql-tag'; //eslint-disable-line

export const state = () => ({
  menus: {
    main: [],
    footer: [],
  },
  options: {},
  meta: {},
})

export const mutations = {
  HYDRATE(state, data) {
    Object.assign(state, { ...state, ...data });
  },
};

export const actions = {
  async nuxtServerInit({ commit, dispatch }) {
    const response = await this.app.apolloProvider.defaultClient.query({
      query: gql`query menus {
        mainMenu: menuItems(where: {location: PRIMARY}) {
          nodes {
            id
            path
            label
          }
        }
        footerMenu: menuItems(where: {location: FOOTER}) {
          nodes {
            id
            path
            label
          }
        }
        general: generalSettings {
          title
        }
        options: globalOptions {
          data {
            companyname
            address
            email
            phone
            linkedin
            instagram
            youtube
            newsletterForm
          }
          meta {
            smrSeoFallbackMetadescription
            smrSeoTwitterhandle
            smrSeoSomeImageFallback {
              id
              mediaDetails {
                sizes {
                  fileSize
                  sourceUrl
                  width
                  height
                  name
                }
              }
            }
          }
        }
      }`
    });

    const data = {
      menus: {
        main: response.data.mainMenu.nodes,
        footer: response.data.footerMenu.nodes,
      },
      options: response.data.options.data,
      meta: { ...response.data.options.meta, ...response.data.general },
    }

    commit('HYDRATE', data);
  },
};
