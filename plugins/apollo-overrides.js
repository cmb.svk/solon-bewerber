const apolloOverrides = ({ app }) => {
  // disable caching on all the queries
  app.apolloProvider.defaultClient.defaultOptions = {
    query: {
      fetchPolicy: process.client ? 'cache-first' : 'no-cache',
    },
  };
};

export default apolloOverrides;