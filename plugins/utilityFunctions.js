import Vue from 'vue';

const stripPTags = (input) => input ? input.replace(/(<p[^>]+?>|<p>|<\/p>)/img, '') : '';

Vue.prototype.$stripPTags = stripPTags;
