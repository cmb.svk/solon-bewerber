import Vue from 'vue';
import VueMq from 'vue-mq';
import styles from '@/assets/scss/exports.scss';

let lastKey = '';
const breakpoints = {};
Object.keys(styles).forEach((key, index) => {
  if (key.startsWith('breakpoint-')) {
    if (index) {
      breakpoints[lastKey] = parseInt(styles[key].replace('strip-unit(', ''));
    }
    lastKey = key.split('-')[1];

    if (index === Object.keys(styles).length - 1) {
      breakpoints[lastKey] = Infinity;
    }
  }
});

const defaultBreakpoint = Object.keys(breakpoints)[0];

Vue.use(VueMq, {
  breakpoints,
  defaultBreakpoint,
});
