import gql from 'graphql-tag';
import head from './head';

export default (apolloQuery, vars = {}, prefetch = true) => {
  return {
    async asyncData({ app, route, error }) {
      const client = app.apolloProvider.defaultClient;
      let data = { data: null };

      await client.query({
        query: gql`${require(`~/apollo/queries/${apolloQuery}`)}`,
        variables: {
          uri: vars.uri || route.path,
          idType: vars.idType || 'URI',
          preview: vars.preview || false,
        },
        prefetch,
      }).then((result) => {
        const name = Object.keys(result.data)[0];

        if (!result.data[name]) {
          error({ statusCode: 404, message: 'Seite nicht gefunden' });
        }

        data = result.data[name];
      }).catch((err) => {
        console.log(err);
        error(err);
      });

      return data;
    },
    head() {
      return head(
        this.acfMeta,
        this.title,
        this.$store.state.meta,
        this.$route
      );
    },
  };
};
