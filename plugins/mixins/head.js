export default (meta, title, store, route) => {
    if (meta) {
        let ogImage = '';
        if (meta.smrSeoSomeImage) {
            ogImage = meta.smrSeoSomeImage !== null ?
                meta.smrSeoSomeImage.mediaDetails.sizes.filter(size => size.name === 'socialmedia')[0] :
                store.smrSeoSomeImageFallback !== null ?
                    store.smrSeoSomeImageFallback.mediaDetails.sizes.filter(size => size.name === 'socialmedia')[0]
                    : {};
        }

        let canonical = route ? route.path : '';

        if (meta && meta.smrSeoCanonical) {
            canonical = meta.smrSeoCanonical !== null ? meta.smrSeoCanonical.url : route.path;
        }

        return {
            titleTemplate: `%s - ${store.title}`,
            title: meta.smrSeoTitle || title,
            meta: [
                {
                    hid: 'description',
                    name: 'description',
                    content: meta.smrSeoMetadescription || store.smrSeoFallbackMetadescription,
                },
                {
                    hid: 'robots',
                    name: 'robots',
                    content: meta.smrSeoIndex ? 'index' : 'noindex',
                },
                {
                    hid: 'og:site_name',
                    property: 'og:site_name',
                    content: store.title || '',
                },
                {
                    hid: 'og:title',
                    property: 'og:title',
                    content: meta.smrSeoTitle || title,
                },
                {
                    hid: 'og:image',
                    property: 'og:image',
                    content: `${ogImage.sourceUrl}?type=original` || '',
                },
                {
                    hid: 'og:description',
                    property: 'og:description',
                    content: meta.smrSeoSomeDescription || store.smrSeoFallbackMetadescription || '',
                },
                {
                    hid: 'og:image:width',
                    property: 'og:image:width',
                    content: ogImage.width || '',
                },
                {
                    hid: 'og:image:height',
                    property: 'og:image:height',
                    content: ogImage.height || '',
                },
                {
                    hid: 'twitter:card',
                    name: 'twitter:card',
                    content: 'summary',
                },
                {
                    hid: 'twitter:site',
                    name: 'twitter:site',
                    content: store.smrSeoTwitterhandle || '',
                },
                {
                    hid: 'twitter:image',
                    name: 'twitter:image',
                    content: `${ogImage.sourceUrl}?type=original` || '',
                },
                {
                    hid: 'apple-mobile-web-app-title',
                    name: 'apple-mobile-web-app-title',
                    content: meta.smrSeoTitle || title || '',
                },
            ],
            link: [
                {
                    hid: 'canonical',
                    rel: 'canonical',
                    href: canonical || '',
                },
            ],
        };
    }
};
