import { onError } from '@apollo/client/link/error'
import cache from './cache';

const link = onError(({ graphQLErrors, networkError, error, operation, response }) => {
    if (graphQLErrors) {
      console.log('APOLLO ERROR:');
      graphQLErrors.forEach(error => { console.log(error); })
    }

    if (networkError) {
      console.log('APOLLO NETWORK ERROR:')
      console.log(networkError)
    }

    if (error) {
      console.log('APOLLO ERROR:');
      console.log(error)
    }

    response.errors = null;
})

export default function (ctx) {
  // const token = process.env.token;
  return {
    httpEndpoint: ctx.$config.APIBASEURL || 'http://homestead.test/cms/graphql',
    httpLinkOptions: {
      credentials: 'include',
    },
    // getAuth: () => `Bearer ${token}`,
    cache,
    persisting: false,
    link,
  }
}
