require('dotenv').config()

const serverconfigStage = {
  host: process.env.SERVER_HOST || 'localhost',
  port: process.env.SERVER_PORT || 3000,
};

const serverconfigLive = {
  host: process.env.SERVER_HOST || 'localhost',
  socket: process.env.PORT || false,
};

export default {

  server: process.env.LIVE === 'true' ? serverconfigLive : serverconfigStage,

  modern: process.env.NODE_ENV === 'production',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    htmlAttrs: {
      lang: 'de',
    },
    titleTemplate: '%s - Solon',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, maximum-scale=1',
      },
      {
        hid: 'description',
        name: 'description',
        content: '*** SEO DESCRIPTIION HERE ***',
      },
      {
        hid: 'og:site_name',
        property: 'og:site_name',
        content: '*** SEO SITENAME HERE ***',
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content:
          '*** SOME SITENAME HERE ***',
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: '*** PATH TO SOME IMAGE HERE ***',
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content:
          '*** SOME DESCRIPTIION HERE ***',
      },
      {
        hid: 'og:image:width',
        property: 'og:image:width',
        content: '1200',
      },
      {
        hid: 'og:image:height',
        property: 'og:image:height',
        content: '630',
      },
      {
        hid: 'twitter:card',
        name: 'twitter:card',
        content: 'summary',
      },
      {
        hid: 'twitter:site',
        name: 'twitter:site',
        content: '*** TWITTER HANDLE HERE ***',
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content: '*** PATH TO SOME IMAGE HERE ***',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      {
        innerHTML: `
        var MTUserId='b821459e-0c14-426d-afa2-293c650d2ea6';
        var MTFontIds = new Array();

        MTFontIds.push("5203659"); // Americana® W05 Bold
        (function() {
            var mtTracking = document.createElement('script');
            mtTracking.type='text/javascript';
            mtTracking.async='true';
            mtTracking.src='/fonts/mtiFontTrackingCode.js';

            (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(mtTracking);
        })();`,
        type: 'text/javascript',
      },
      {
        id: 'Cookiebot',
        src: 'https://consent.cookiebot.com/uc.js',
        'data-cbid': 'e5b8aca2-fb5f-425a-8bb7-f47a64b8a88f',
        type: 'text/javascript',
        async: true,
      },
    ],
    __dangerouslyDisableSanitizers: ['script'],
  },

  publicRuntimeConfig: {
    YBUG: process.env.YBUG || false,
    APIBASEURL: process.env.NODE_ENV !== 'production' ? process.env.APIBASEURL || 'https://solonadmin.qa.schumacher-aktuell.de/cms/graphql' : 'http://homestead.test/cms/graphql',
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/vue-mq.js',
    '@/plugins/utilityFunctions.js',
    '@/plugins/apollo-overrides.js',
    '@/plugins/vue-youtube-embed.js',
    { src: '@/plugins/vue-lazyload.js', ssr: false },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: ['~/components/'],

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    {
      src: '@/assets/scss/app.scss',
      lang: 'scss',
    },
  ],

  // Nuxt Style Resources (https://github.com/nuxt-community/style-resources-module#readme)
  styleResources: {
    scss: ['@/assets/scss/resources.scss'],
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    // 'bootstrap-vue/nuxt',
    // https://github.com/nuxt-community/style-resources-module
    '@nuxtjs/style-resources',
    // https://www.npmjs.com/package/@nuxtjs/apollo
    '@nuxtjs/apollo',
    // https://github.com/vanhoofmaarten/nuxt-mq
    // 'nuxt-mq',
    '@nuxtjs/sitemap',
    'nuxt-leaflet',
    'nuxt-basic-auth-module',
  ],

  // BootstrapVue (https://bootstrap-vue.org/docs#nuxt-js)
  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false,
    icons: false,
  },

  // Apollo https://www.npmjs.com/package/@nuxtjs/apollo
  apollo: {
    clientConfigs: {
      default: '~/apollo/config.js',
    },
    defaultOptions: {
      // See 'apollo' definition
      // For example: default query options
      $query: {
        loadingKey: 'loading',
        fetchPolicy: 'no-cache', // doesn't work here, change in apollo-overrides plugin!!!
      },
    },
  },

  basic: {
    name: 'solon',
    pass: 'SchweineDrache',
    enabled: process.env.NODE_ENV === 'production' && process.env.LIVE !== 'true',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['vue-youtube-embed'],
    analyze: false,
    extractCSS: false,
    splitChunks: {
      layouts: true,
      pages: true,
      commons: true,
    },
    // limit chunk size
    // extend(config, { isClient }) {
    //   const isProd = process.env.NODE_ENV === 'production';
    //   if (isProd && isClient) {
    //     config.optimization.splitChunks.maxSize = 249856; // 244 Kib
    //   }
    // },
  },

  loading: {
    color: '#e500aa',
    height: '5px'
  },
}
